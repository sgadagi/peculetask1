package com.sachingadagi.peculetask1.adapters;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.sachingadagi.peculetask1.models.Book;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import sachingadagi.com.peculetask1.R;

/**
 * Created by tuxer on 3/16/2016.
 */
public class BookListAdapter extends BaseAdapter implements Filterable {
    Context context;
    ArrayList<Book> books = new ArrayList<Book>();
    List<Book> filteredBooksList = null;
    Resources res ;
    CharSequence search_key = "";
    public BookListAdapter(Context context) {

        this.context = context;
        res = context.getResources();
        for (int i = 0; i < 8; i++)
        {

        Book book = new Book();
        book.setTitle(res.getStringArray(R.array.titles)[i]);
        book.setAuthor(res.getStringArray(R.array.authors)[i]);
        book.setPublisher(res.getStringArray(R.array.publishers)[i]);

        books.add(book);
        filteredBooksList = books;
        }
    }


    @Override
    public int getCount() {
        return filteredBooksList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredBooksList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater  inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View row = inflater.inflate(R.layout.book_row,parent,false); //TODO

        TextView title = (TextView) row.findViewById(R.id.title);
        TextView author = (TextView) row.findViewById(R.id.author);
        TextView publisher = (TextView) row.findViewById(R.id.publisher);

        // Partial coloring of TV- for title
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder(filteredBooksList.get(position).getTitle());
        Pattern searchPattern = Pattern.compile(search_key.toString(),Pattern.CASE_INSENSITIVE);
        Matcher matcher  = searchPattern.matcher(filteredBooksList.get(position).getTitle());
        while (matcher.find())
        {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)), matcher.start(), matcher.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        title.setText(spannableStringBuilder);

        // Partial coloring of TV- for Author
        spannableStringBuilder = new SpannableStringBuilder(filteredBooksList.get(position).getAuthor());
        searchPattern = Pattern.compile(search_key.toString(),Pattern.CASE_INSENSITIVE);
        matcher  = searchPattern.matcher(filteredBooksList.get(position).getAuthor());
        while (matcher.find())
        {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)), matcher.start(), matcher.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        author.setText(spannableStringBuilder);

        // Partial coloring of TV- for publisher
        spannableStringBuilder = new SpannableStringBuilder(filteredBooksList.get(position).getPublisher());
        searchPattern = Pattern.compile(search_key.toString(),Pattern.CASE_INSENSITIVE);
        matcher  = searchPattern.matcher(filteredBooksList.get(position).getPublisher());
        while (matcher.find())
        {
            spannableStringBuilder.setSpan(new ForegroundColorSpan(Color.rgb(255, 0, 0)), matcher.start(), matcher.end(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }
        publisher.setText(spannableStringBuilder);

        return row;
    }

    @Override
    public Filter getFilter() {

        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                List<Book> filteredBooks = new ArrayList<Book>();




                if(!constraint.toString().equals("") && constraint.toString().length() > 0)
                {
                    Log.i("PECULE","not empty");
                    for(Book b : books ){
                        if(b.getAuthor().toLowerCase().contains(constraint.toString().toLowerCase()) || b.getTitle().toLowerCase().contains(constraint.toString().toLowerCase()) || b.getPublisher().toLowerCase().contains(constraint.toString().toLowerCase())){
                           filteredBooks.add(b);
                        }
                    }
                    filterResults.count = filteredBooks.size();
                    filterResults.values = filteredBooks;
                }
                else
                {
                    Log.i("PECULE","empty");
                    filterResults.count = books.size();
                    filterResults.values = books;
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {

            filteredBooksList = (List<Book>) results.values;
                search_key = constraint;
                notifyDataSetChanged();
            }
        };
        return filter;
    }

}
